 -- SUMMARY --
Amazing Bird is responsive and clean theme. It would be a good pick for
corporate, company, blog, product showcase and personal websites. This is very
light weight with simple layout, built using HTML5, CSS3 and jQuery. With this
theme, you can set up a site and launch it quickly.

Features
 * Fully Responsive
 * Light weight (no frameworks used)
 * Banner is customizable, three options is provided, 
   1. Image and text 2. Text Only 3.slideshow(capable of max five slides)
 * HTML5 Markup
 * 18 Regions to use

TODO
Support for multilevel menu

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure Banner settings
  - Navigate to admin/appearance/settings/amazing_bird
  - In the form, locate "Amazing Bird Banner Setting"
    - In "Banner Layout type" choose one style from three radio buttons. Each
      option has a form below to capture the Title, Body, URL and image.
    - Click "Save Configuration" to save.

 -- CUSTOMIZATION --

 * For slideshow, slick slider plugin is used, slick plugin files are added
   through amazing_bird.libraries.yml, CDN URL are used.
 * if you want to host the files in your local, you can do it, by changing file
   path in "slick-slider" definition in amazing_bird.libraries.yml

 -- CONTACT --

Current maintainers:
* Kamalakannan (kannan@kiluvai.com) - https://www.drupal.org/user/168271

This project has been sponsored by:
 * Kiluvai 
   - A web development company specialised in Drupal,
     visit http://www.kiluvai.com
