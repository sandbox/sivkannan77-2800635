/**
 * @file
 * Custom js for amazing_bird.
 */

(function ($, Drupal) {

  'use strict';

  $('.region-primary-menu .menu-toggle a').click(function (event) {
    $('.region-primary-menu .links').stop(true, true).slideToggle();
  });

})(jQuery, Drupal);
