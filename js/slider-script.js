/**
 * @file
 * Custom js for calling slick slider function.
 */

(function ($, Drupal) {

  'use strict';

  $('.home-slider').slick();

})(jQuery, Drupal);
